
public interface Observateur {

    void metsAJour(String attributModifie,
    Object nouvelleValeur);

    String getModification();
    
    String getNom();
}
