import java.util.*;

public class Test {
    
    private Test () { };

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Point unPoint = new Point(10, 15);
        System.out.println("Point: (" + unPoint.getX() + ", " + unPoint.getY() + ")");
        String reponse;
        Observateur obs1 = new ObservateurCoordonnee("obs1");
        unPoint.enregistreObservateur(obs1);
        unPoint.enregistreObservateur(new ObservateurCoordonnee("obs2"));
        unPoint.enregistreObservateur(new ObservateurCoordonnee("obs2"));
        do {
            System.out.println("Tapez :");
            System.out.println(" x pour modifier X\n y pour modifier Y\n q pour quitter");
            reponse = reader.next().trim().toUpperCase();
            if (reponse.equals("Q")) { break; }
            
            if (reponse.equals("X")) {
                System.out.print("Entrez la coordonnée X : ");
                Integer x = reader.nextInt();
                unPoint.setX(x);
            } else if (reponse.equals("Y")) {
                System.out.print("Entrez la coordonnée Y : ");
                Integer y = reader.nextInt();
                unPoint.setY(y);
            }
            System.out.println("Point: (" + unPoint.getX() + ", " + unPoint.getY() + ")");
            System.out.println("Nom : " + obs1.getNom() );
            System.out.println(" Observation : " + obs1.getModification());
        } while (true);
    }

}
